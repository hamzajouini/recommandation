<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Http;
use predictionio\EventClient;

use App\Recommandation;

class PredictionIOCommand extends Command
{
    private $ip = "http://192.168.43.62";

    private $Acceskey ='sN1CBW5-Kc0CT5gL1QvFEmtxydDUKJilv-I88zerej0LVkw2P_U_jUcpFZzecSMf';
    private $url ="http://192.168.43.62:7070";

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'predictionIO:fetch';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'PredictionIO fetch job';



    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        echo "PredcitionION process started\n";
        
        $this->viewRecorder();
         $this->buyRecorder();
         $this->addUsers();
         $this->addItem();
         $this->loadCustomers();
         echo "PredictionIO process finished\n";
    }


    private  function viewRecorder()
    {  
        echo "viewRecorder started\n";
        $response = Http::get("{$this->ip}:80/rest/V1/pfe-test/trackingView");
        $result = $response->json();
        $client = new EventClient($this->Acceskey,$this->url);
        $event ='view' ;
        foreach($result as $object) {
            $uid = intval($object['customer_id']);
            $iid = intval($object['product_id']);
            $client->recordUserActionOnItem($event,$uid,$iid);       
        }
        echo "viewRecorder finished\n";
    }


    private function buyRecorder()
    {   
        echo "buyRecorder started\n";
        $response = Http::get("{$this->ip}:80/rest/V1/pfe-test/trackingBuy");
        $result = $response->json();    
        $client = new EventClient($this->Acceskey,$this->url);
        $event ='buy' ;
        foreach($result as $object) {
            $uid = intval($object['customer_id']);
            $productsIds = $object['items'];
            foreach($productsIds as $iid) {
                $client->recordUserActionOnItem($event,$uid,intval($iid));
            }
        }
        echo "buyRecorder finished\n";
    }


    private  function addUsers()
    {    

        echo "addUsers started\n";
        $response = Http::get("{$this->ip}:80/rest/V1/pfe-test/customers");
        $result = $response->json();
        $client = new EventClient($this->Acceskey,$this->url);
        foreach($result as $key => $ID) {
          // Create a new user
        $client->setUser($ID);
        }
        echo "addUsers finished\n";
    }


    private function addItem()
    {    
        echo "addItem started\n";
        $response = Http::get("{$this->ip}:80/rest/V1/pfe-test/products");
        $client = new EventClient($this->Acceskey,$this->url);
        $result = $response->json();
         foreach($result as $object){                          
            $client->setItem($object['product_id']);
        }

        echo "addItem finished\n";
    }


    public function loadCustomers() {
        echo "Start saving recommendation to DB \n";
        $response = Http::get("{$this->ip}:80/rest/V1/pfe-test/customers")->json();
        foreach($response as $customerId) {
            $this->getRecommendations($customerId, 10);
        }
    }

    private function getRecommendations($user, $num)
    {
       $response = Http::post("{$this->ip}:8000/queries.json", [
           "user" => $user,
           "num" => $num
       ])->json();
       
       $items = []; 
       foreach($response["itemScores"] as $object) {
           $items[] = $object["item"];
       }


       $recommendation = new Recommandation();
       $recommendation->items = join(",", $items);
       $recommendation->customer = $user;
       $recommendation->save();
       echo "Recommendation has been saved for user $user\n";
    }
}