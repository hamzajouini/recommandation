<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use App\Recommandation;
class DashboardController extends Controller
{
    private $ip = "http://192.168.43.62";

    public function getProducts()
    {
       $products = Http::get("{$this->ip}:80/rest/V1/pfe-test/mostviewedproducts")->json();
       return view("dashboard")->with(["products" => $products]);
       
    }
}
