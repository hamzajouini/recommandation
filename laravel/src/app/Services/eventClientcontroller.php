<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use predictionio\EventClient;
class eventClientcontroller extends Controller
{
    private $Acceskey ='sN1CBW5-Kc0CT5gL1QvFEmtxydDUKJilv-I88zerej0LVkw2P_U_jUcpFZzecSMf';
    private $url ='http://192.168.43.124:7070';
    
    /**
     * @Route("/eventUser/add")
     */
    
    public function addUser()
    {    
        $clients = HttpClient::create();
        $response = $clients->request('GET','http://192.168.43.124:80/rest/V1/pfe-test/customers');
        $result = $response->toArray();
        $client = new EventClient($this->Acceskey,$this->url);
        foreach($result as $key => $ID) {
          // Create a new user
        $client->setUser($ID);
        }
        return $this->json( ['finish']);
    }


    /**
     * @Route("/eventUser/delete/{ID}", methods={"GET"})
     */

   public function delete($ID)
   {
    $client = new EventClient($this->Acceskey,$this->url);
    
    try {
      $result = $client->deleteUser($ID);
      return $this->json([
        "data" => $result
      ]);
    }catch(\PredictionIOAPIError $e) {
      return $this->json([
        "data" => $e->getMessage()
      ]);
    }
  
   }
}
